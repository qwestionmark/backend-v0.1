# frozen_string_literal: true

FactoryBot.define do
  factory :customfield do
    # Need to set 2 parameters !
    # resource_id: ID of an existing object
    # resource_type: type of an existing object, defaults to Project

    resource_type { 'Project' }
    name { FFaker::Movie.title }
    description { FFaker::DizzleIpsum.paragraph }
    field_type { FFaker::Movie.title }
    optional { FFaker::Boolean }
  end
end
