# frozen_string_literal: true

FactoryBot.define do
  factory :interest do
    interest_name { FFaker::Movie.title }
  end
end
