# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Relation, type: :model do
  describe 'association' do
    it { should belong_to(:user) }
  end

  describe 'validation' do
    it 'must be owned by a user' do
      relation = Relation.create
      expect(relation.errors.full_messages).to include('User must exist')
    end
  end
end
