# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Membership, type: :concern do
  let(:dummy) { Class.new.extend(described_class) }

  describe '#members_count' do
    it 'returns a count of resource members' do
      expect(dummy).to receive(:all_owners_admins_members) { double('dummy', active: [2, 4]) }
      expect(dummy.members_count).to eq(2)
    end
  end
end
