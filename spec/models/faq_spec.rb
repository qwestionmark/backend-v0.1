# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Faq, type: :model do
  describe 'validation' do
    it 'should have valid factory' do
      expect(build(:faq)).to be_valid
    end
  end

  describe 'has documents' do
    it 'that can be added' do
      document = create(:document)
      faq = create(:faq)
      count = faq.documents.count
      faq.documents << document
      expect(faq.documents.count).to eq(count + 1)
      expect(faq.documents.pluck(:id)).to include document.id
    end
  end

  describe 'has_faqable' do
    it 'can be attached to a program' do
      program = create(:program)
      faq = create(:faq)
      program.faq = faq
      expect(program.faq).to be faq
      expect(faq.faqable).to be program
    end
  end
end
