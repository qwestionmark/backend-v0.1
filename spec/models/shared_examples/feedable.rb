# frozen_string_literal: true

RSpec.shared_examples 'a model that can have a feed' do |factory|
  describe '#create_feed' do
    it 'triggers create feed on create' do
      model = build(factory)
      expect(model).to receive(:create_feed).and_call_original
      model.save
      expect(model.feed).to be_present
    end
  end

  describe 'posts_count' do
    it "delegates post_count to the model's feed" do
      model = create(factory)
      user = create(:user)

      model.feed.posts << Post.create!(feed: model.feed, user: user, from: model.feed.feedable, content: 'whatever')
      expect(model.posts_count).to eq(1)
    end
  end
end
