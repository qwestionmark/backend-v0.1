# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_examples 'an object with followable' do |factory|
  before do
    @object = create(factory)
  end

  context 'Signed in' do
    before do
      @user = create(:confirmed_user, first_name: 'Sandara', last_name: 'Fragado')
      sign_in @user
    end

    describe '#follow' do
      it "should follow the #{described_class}" do
        put :follow, params: { id: @object.id }
        expect(response).to have_http_status :ok
        expect(@object.followers.count).to eq 1
        expect(RecsysDatum.where(sourceable_node: @user, targetable_node: @object, relation_type: 'has_followed')).to exist
      end

      it 'should trigger a notification' do
        expect do
          put :follow, params: { id: @object.id }
        end.to change(Notification, :count).by(1)
        notification = Notification.last
        case @object.class.name
        when 'User'
          expect(notification.type).to match('new_user_follower')
          expect(notification.target).to be == @object
          expect(notification.object).to be == @user
          expect(notification.subject_line).to eq('Sandara Fragado now follows you!')
        when 'Community'
          expect(notification.type).to match('new_follower')
          expect(notification.target).to be == @object.users.first
          expect(notification.object).to be == @object
          expect(notification.subject_line).to eq("Sandara Fragado now follows your group: #{@object.title}")
        else
          expect(notification.type).to match('new_follower')
          expect(notification.target).to be == @object.users.first
          expect(notification.object).to be == @object
          expect(notification.subject_line).to eq("Sandara Fragado now follows your #{@object.class.to_s.downcase}: #{@object.title}")
        end
        expect(notification.category).to match('follow')
        expect(notification.metadata[:author_id]).to be(@user.id)
      end
    end

    describe '#unfollow' do
      before do
        @relation = @user.owned_relations.create(resource: @object, follows: true)
        @user.add_edge(@object, 'has_followed')
      end

      it "should unfollow the #{described_class}" do
        expect(@object.followers.count).to eq 1
        delete :follow, params: { id: @object.id }
        expect(response).to have_http_status :ok
        expect(@object.followers.count).to eq 0
        expect(RecsysDatum.where(sourceable_node: @user, targetable_node: @object, relation_type: 'has_followed')).not_to exist
      end
    end

    describe '#is_follow' do
      before do
        @relation = @user.owned_relations.create(resource: @object, follows: true)
      end

      it 'should answer true' do
        get :follow, params: { id: @object.id }
        json_response = JSON.parse(response.body)
        expect(response).to have_http_status :ok
        expect(json_response['has_followed']).to eq true
      end

      it 'should answer false' do
        @relation.follows = false
        @relation.save
        get :follow, params: { id: @object.id }
        json_response = JSON.parse(response.body)
        expect(response).to have_http_status :ok
        expect(json_response['has_followed']).to eq false
      end

      it 'should answer not_found' do
        @relation.delete
        get :follow, params: { id: @object.id }
        json_response = JSON.parse(response.body)
        expect(response).to have_http_status :ok
        expect(json_response['has_followed']).to eq false
      end
    end

    describe '#followers' do
      before do
        @relation = @user.owned_relations.create(resource: @object, follows: true)
      end

      it "should get the followers of the #{described_class}" do
        get :followers, params: { id: @object.id }
        json_response = JSON.parse(response.body)
        expect(response).to have_http_status :ok
        expect(json_response['followers'].count).to eq 1
        expect(json_response['followers'][0]['projects_count']).to eq @user.projects_count if @object.class.name == 'User'
      end
    end
  end

  # Testing the Auth methods

  context 'Not Signed In' do
    it 'should require auth' do
      get :follow, params: { id: @object.id }
      expect(response).to have_http_status :unauthorized
    end

    it 'should require auth' do
      put :follow, params: { id: @object.id }
      expect(response).to have_http_status :unauthorized
      expect(@object.followers.count).to eq 0
    end

    it 'should require auth' do
      delete :follow, params: { id: @object.id }
      expect(response).to have_http_status :unauthorized
      expect(@object.followers.count).to eq 0
    end
  end
end
