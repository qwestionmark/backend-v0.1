# frozen_string_literal: true

require 'rails_helper'
# require 'sidekiq/testing'

RSpec.describe UserJoinedEmailWorker, type: :worker do
  describe 'testing worker' do
    # it "should respond to perform" do
    #   owner = build(:user)
    #   object = build(:project, creator_id: owner.id)
    #   joiner = create(:user)
    #   expect(UserJoinedMailer.new).to respond_to(:perform)
    # end

    # describe "Perform" do
    #   before do
    #     Sidekiq::Worker.clear_all
    #   end
    # end

    it 'should sends to right email' do
      owner = build(:user)
      object = build(:project, creator_id: owner.id)
      joiner = create(:user)
      mail = UserJoinedMailer.user_joined(owner, object, joiner)
      expect(mail.to).to eq [owner.email]
    end

    it 'should renders the subject' do
      owner = build(:user)
      object = build(:project, creator_id: owner.id)
      joiner = create(:user)
      mail = UserJoinedMailer.user_joined(owner, object, joiner)
      expect(mail.subject).to eq 'A user joined your project'
    end
  end
end
