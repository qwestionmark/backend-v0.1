# frozen_string_literal: true

module NotificationPusher
  module DeliveryMethod
    class EmailPusher < NotificationPusher::DeliveryMethod::Base
      def call
        if notification.target.settings.delivery_methods!.email!.enabled.nil? || notification.target.settings.categories.send("#{notification.category}!").delivery_methods!.email!.enabled.nil?
          notification.deliver_email
        elsif notification.target.settings.delivery_methods!.email!.enabled && notification.target.settings.categories.send("#{notification.category}!").delivery_methods!.email!.enabled
          notification.deliver_email
        end
        # `notification` and `options` are accessible here
      end
    end
  end
end

NotificationPusher.configure do |config|
  # A delivery method handles the process of sending your notifications to
  # various services for you.
  # Learn more: https://github.com/jonhue/notifications-rails/tree/master/notification-pusher#delivery-methods
  config.register_delivery_method :email, :EmailPusher
end
