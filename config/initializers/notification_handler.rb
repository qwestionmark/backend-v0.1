# frozen_string_literal: true

require 'notification-handler'

NotificationHandler.configure do |config|
  # Cache amount of unread and read notifications for notification targets.
  # Learn more: https://github.com/jonhue/notifications-rails/tree/master/notification-handler#caching
  # config.cache = false

  # Groups are a powerful way to bulk-create notifications for multiple objects
  # that don't necessarily have a common class.
  # Learn more: https://github.com/jonhue/notifications-rails/tree/master/notification-handler#groups
  # config.define_group :subscribers, -> { User.where(subscriber: true) }

  config.define_group :members, ->(object) { User.with_role(:member, object) }
  config.define_group :admins, ->(object) { User.with_role(:admin, object) }
  config.define_group :admins_minus_poster, ->(object, post) { User.with_role(:admin, object) - [post.user] }
  config.define_group :owners, ->(object) { User.with_role(:owner, object) }
  config.define_group :feed_host_followers_minus_post_author, ->(post, feed_host) { feed_host.followers - [post.user] }
  config.define_group :post_participants_except_commenter, ->(post, comment) { post.participants_except_commenter(comment) }
  config.define_group :everyone, -> { User.active }
  config.define_group :user_followers, ->(user) { user.followers }
end
