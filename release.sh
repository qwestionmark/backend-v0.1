#!/bin/sh

# Adapted from https://toedter.com/2018/06/02/heroku-docker-deployment-update/

docker pull $BACKEND_IMAGE
docker pull $SIDEKIQ_IMAGE
docker tag $BACKEND_IMAGE registry.heroku.com/$HEROKU_APP/web
docker tag $SIDEKIQ_IMAGE registry.heroku.com/$HEROKU_APP/sidekiq
echo $HEROKU_TOKEN | docker login -u _ --password-stdin registry.heroku.com
docker push registry.heroku.com/$HEROKU_APP/web
docker push registry.heroku.com/$HEROKU_APP/sidekiq
apk add --no-cache curl

imageId=$(docker inspect registry.heroku.com/$HEROKU_APP/web --format={{.Id}})
payload='{"updates":[{"type":"web","docker_image":"'"$imageId"'"}]}'

curl -n -X PATCH https://api.heroku.com/apps/$HEROKU_APP/formation \
-d "$payload" \
-H "Content-Type: application/json" \
-H "Accept: application/vnd.heroku+json; version=3.docker-releases" \
-H "Authorization: Bearer $HEROKU_TOKEN"

curl -n -X POST https://api.heroku.com/apps/$HEROKU_APP/dynos \
	-H "Content-Type: application/json" \
	-H "Accept: application/vnd.heroku+json; version=3" \
	-H "Authorization: Bearer ${HEROKU_TOKEN}" \
	-d '{ "command": "bundle exec rails db:migrate db:seed db:seed:interests", "type": "run"}'

imageId=$(docker inspect registry.heroku.com/$HEROKU_APP/sidekiq --format={{.Id}})
payload='{"updates":[{"type":"sidekiq","docker_image":"'"$imageId"'"}]}'

curl -n -X PATCH https://api.heroku.com/apps/$HEROKU_APP/formation \
-d "$payload" \
-H "Content-Type: application/json" \
-H "Accept: application/vnd.heroku+json; version=3.docker-releases" \
-H "Authorization: Bearer $HEROKU_TOKEN"
