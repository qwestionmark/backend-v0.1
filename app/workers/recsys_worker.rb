# frozen_string_literal: true

class RecsysWorker
  include Sidekiq::Worker

  def deliver_recommendations
    # The RECSYS_ALLOWLIST environment variable can contain a comma separated list
    # of user ids that are allowed to receive recsys emails.  This feature
    # is only needed during the alpha and beta rollouts of the recommendation
    # system and needs to be removed once it is ready for a production rollout.

    users = if ENV['RECSYS_ALLOWLIST'].present?
      user_ids = ENV['RECSYS_ALLOWLIST'].split(',').map(&:strip)
      User.confirmed.where(id: user_ids)
    else
      User.confirmed
    end

    users.find_each do |user|
      RecsysUserWorker.perform_async(user.id)
    end
  end

  def perform
    # Create new recommendations
    return unless system('python3 -m jogl.recsys --config recsys.yaml')
    deliver_recommendations
  end
end
