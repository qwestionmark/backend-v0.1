# frozen_string_literal: true

class RecsysUserWorker
  include Sidekiq::Worker

  def perform(user_id)
    user = User.find(user_id)
    deliver_recommendation(user)
  end

  def deliver_recommendation(user)
    RecsysNeedsFinder.new(user.id).call
  end
end
