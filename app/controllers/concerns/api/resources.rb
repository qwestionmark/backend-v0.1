# frozen_string_literal: true

module Api::Resources
  extend ActiveSupport::Concern

  included do
    before_action :resource_authenticate_user!, only: %i[add_resource update_resource remove_resource]
    before_action :resource_is_admin!, only: %i[add_resource update_resource remove_resource]
    before_action :find_resource, only: %i[update_resource remove_resource]
  end

  def index_resource
    render json: @obj.resources, each_serializer: Api::DocumentSerializer, status: :ok
  end

  def add_resource
    @document = Document.new(resource_params)
    if @document.save
      @obj.resources << @document
      render json: @obj.resources, each_serializer: Api::DocumentSerializer, status: :created
    else
      render json: @document.errors, status: :unprocessable_entity
    end
  end

  def update_resource
    if @document.update(resource_params)
      @obj.resources << @document
      render json: @obj.resources, each_serializer: Api::DocumentSerializer, status: :created
    else
      render json: @document.errors, status: :unprocessable_entity
    end
  end

  def remove_resource
    if @document.destroy
      render json: @obj.resources, each_serializer: Api::DocumentSerializer, status: :ok
    else
      render json: { "error": 'Something went wrong...' }, status: :unprocessable_entity
    end
  end

  private

  def resource_authenticate_user!
    authenticate_user!
  end

  def resource_is_admin!
    render json: { error: 'Only an admin can add edit or delete a resource' }, status: :forbidden unless current_user.has_role?(:admin, @obj) && return
  end

  def resource_params
    params.require(:resource).permit(:title, :content)
  end

  def find_resource
    @document = Document.find_by(id: params[:resource_id])
    render json: { data: "Resource id:#{params[:resource_id]} was not found" }, status: :not_found if @document.nil?
  end
end
