# frozen_string_literal: true

class Api::PagesController < ApplicationController
  def index
    render json: { content: 'Welcome to the JOGL API' }, status: :ok
  end
end
