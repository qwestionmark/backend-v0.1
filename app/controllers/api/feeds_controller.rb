# frozen_string_literal: true

class Api::FeedsController < ApplicationController
  before_action :find_feed, only: %i[show remove_post]
  before_action :authenticate_user!, only: %i[index remove_post]
  before_action :set_obj, only: [:remove_post]
  before_action :is_admin, only: [:remove_post]
  before_action :find_post, only: [:remove_post]

  # before_action :sanitize, only: [:create, :update]
  include Api::Utils

  def index
    _pagy, myfeed = pagy(current_user.timeline_posts)
    render json: myfeed, each_serializer: Api::PostSerializer, status: :ok
  end

  def show
    @pagy, @somefeed = pagy(@feed.posts.order('created_at DESC'))
    render json: @somefeed, each_serializer: Api::PostSerializer, status: :ok
  end

  def indexall
    param = if params[:order] == 'desc'
              'id DESC'
            else
              'id ASC'
            end
    @pagy, @joglfeed = pagy(Post.order(param).all)
    render json: @joglfeed, each_serializer: Api::PostSerializer, status: :ok
  end

  def remove_post
    @feed.posts.delete(@post)
    render json: { data: "Post id:#{params[:post_id]} removed from feed" }, status: :ok
  end

  private

  def set_obj
    @obj = @feed.feedable
  end

  def find_feed
    @feed = Feed.find(params[:id])
    render json: { data: "Feed id:#{params[:id]} not found" }, status: :not_found if @feed.nil?
  end

  def find_post
    @post = Post.find(params[:post_id])
    render json: { data: "Post id:#{params[:post_id]} not found" }, status: :not_found if @post.nil?
  end
end
