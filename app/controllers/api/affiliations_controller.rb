# frozen_string_literal: true

class Api::AffiliationsController < ApplicationController
  before_action :authenticate_user!

  def create
    render(json: { errors: ['Affiliation must be present'] }, status: :unprocessable_entity) && return unless params[:affiliation].present?

    affiliation_parameters = affiliations_params
    affiliate_type = affiliation_parameters[:affiliate_type] = affiliation_parameters[:affiliate_type].to_s.classify.constantize
    parent_type = affiliation_parameters[:parent_type] = affiliation_parameters[:parent_type].to_s.classify.constantize

    parent = parent_type.find(affiliation_parameters[:parent_id])
    affiliate = affiliate_type.find(affiliation_parameters[:affiliate_id])

    affiliation = parent.add_affiliate(affiliate)
    if affiliation.errors.any?
      render json: { errors: affiliation.errors.full_messages }
    else
      json_response(affiliation)
      # render json: { errors: ['Affiliate must be present', 'Parent must be present']}, status: :unprocessable_entity
    end
  end

  def destroy
    Affiliation.find(params[:id]).destroy
  end

  private

  def affiliations_params
    params.require(:affiliation).permit(:affiliate_id, :affiliate_type, :parent_id, :parent_type)
  end
end
