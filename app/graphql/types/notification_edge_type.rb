# frozen_string_literal: true

# Make an edge class for use in the connection below:
class Types::NotificationEdgeType < GraphQL::Types::Relay::BaseEdge
  node_type(::Types::NotificationType)
end
