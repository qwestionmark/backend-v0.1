module Types
  class SpaceStatus < Types::BaseEnum
    value 'DRAFT', "Space hasn't been published yet"
    value 'SOON', 'Space will be published soon'
    value 'ACTIVE', 'Space is active'
    value 'COMPLETED', 'Space is no longer active'
  end
end
