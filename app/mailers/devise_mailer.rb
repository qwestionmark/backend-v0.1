# frozen_string_literal: true

require 'digest/sha2'
class DeviseMailer < Devise::Mailer
  default 'message-id' => "<#{Digest::SHA2.hexdigest(Time.now.to_i.to_s)}@jogl.io>",
          from: "JOGL - Just One Giant Lab <#{ENV['JOGL_NOTIFICATIONS_EMAIL']}>",
          message_stream: 'devise' # Postmark
end
