# frozen_string_literal: true

class UserJoinedMailer < ApplicationMailer
  def user_joined(owner, object, joiner)
    @to = owner
    @owner = owner
    @object = object
    @from_object = object.class.name.downcase != 'community' ? object.class.name.downcase : 'group'
    @joiner = joiner
    # Postmark metadatas
    metadata['user-id-to'] = @joiner.id
    metadata['object-type'] = @from_object
    mail(to: "<#{owner.email}>", subject: 'A user joined your ' + @from_object, tag: 'user-joined')
  end
end
