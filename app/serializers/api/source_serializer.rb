# frozen_string_literal: true

class Api::SourceSerializer < ActiveModel::Serializer
  attributes :id, :title, :url
end
