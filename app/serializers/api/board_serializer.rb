# frozen_string_literal: true

class Api::BoardSerializer < ActiveModel::Serializer

  attributes :id,
             :title,
             :description

  has_many :users
end
