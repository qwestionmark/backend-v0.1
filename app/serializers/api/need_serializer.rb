# frozen_string_literal: true

class Api::NeedSerializer < ActiveModel::Serializer
  include Api::Usersserializerhelper
  include Api::Utilsserializerhelper
  include Api::Relationsserializerhelper
  include Api::Rolesserializerhelper

  attributes :id,
             :title,
             :content,
             :creator,
             :documents,
             :status,
             :feed_id,
             :users_sm,
             :end_date,
             :skills,
             :ressources,
             :project,
             :follower_count,
             :members_count,
             :claps_count,
             :saves_count,
             :posts_count,
             :created_at,
             :updated_at,
             :is_urgent

  attribute :is_owner, unless: :scope?
  attribute :is_admin, unless: :scope?
  attribute :is_member, unless: :scope?
  attribute :has_followed, unless: :scope?
  attribute :has_clapped, unless: :scope?
  attribute :has_saved, unless: :scope?

  def project
    @project = object.project
    if @project.nil?
      {
        id: -1,
        title: object.title,
        banner_url: ""
      }
    else
      {
        id: @project.id,
        title: @project.title,
        banner_url: @project.banner_url
      }
    end
  end

  def show_objects?
    @instance_options[:show_objects]
  end
end
