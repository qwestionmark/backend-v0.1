class Api::SerializerHelper
  include Api::Relationsserializerhelper
  include Api::Rolesserializerhelper
  include Api::Usersserializerhelper
  include Api::Utilsserializerhelper

  attr_writer :context

  def current_user
    @context[:current_user]
  end
end
