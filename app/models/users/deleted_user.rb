module Users
  class DeletedUser
    attr_reader :id,
                :email,
                :first_name,
                :last_name,
                :nickname,
                :logo_url_sm,
                :short_bio,
                :can_contact,
                :projects_count

    def initialize
      @id = -1
      @email = 'deleted@user.com'
      @first_name = 'Deleted'
      @last_name = 'User'
      @nickname = 'deleted_user'
      @logo_url_sm = ''
      @short_bio = ''
      @can_contact = false
      @projects_count = 0
    end

    def logo_url(default = '')
      default
    end

    def follow_mutual_count(_user)
      0
    end
  end
end
