# frozen_string_literal: true

class Ressource < ApplicationRecord
  include AlgoliaSearch
  include RecsysHelpers

  algoliasearch disable_indexing: Rails.env.test? do
    attribute :ressource_name
  end
end
