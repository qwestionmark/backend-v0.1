# frozen_string_literal: true

module Linkable
  extend ActiveSupport::Concern

  included do
    has_many :external_links, as: :linkable, dependent: :destroy
  end
end
