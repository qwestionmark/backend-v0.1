# frozen_string_literal: true

module RecsysHelpers
  extend ActiveSupport::Concern

  def add_edge(target, relation_type)
    edge = RecsysDatum.find_or_create_by!(sourceable_node: self, targetable_node: target, relation_type: relation_type)
    edge.value += 1
    edge.save
  end

  def remove_edge(target, relation_type)
    edge = RecsysDatum.find_by(sourceable_node: self, targetable_node: target, relation_type: relation_type)
    edge&.destroy
  end
end
