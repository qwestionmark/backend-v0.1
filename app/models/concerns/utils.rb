# frozen_string_literal: true

module Utils
  extend ActiveSupport::Concern

  def obj_type
    self.class.name
  end

  def sanitize(text)
    ActionController::Base.helpers.sanitize(
      text,
      options = {
        tags: %w[p br h1 h2 h3 a strong em u s sub sup span img iframe pre blockquote ul li ol],
        attributes: %w[height width style src href rel target class allowfullscreen frameborder data-value data-index data-denotation-char data-link]
      }
    )
  end

  def sanitize_description
    self.description = sanitize(description)
  end

  def sanitize_content
    self.content = sanitize(content)
  end

  def reindex(_anything)
    index! if persisted?
  end
end
