# frozen_string_literal: true

class Mention < ApplicationRecord
  notification_object
  belongs_to :post
  belongs_to :obj, polymorphic: true
  validates_uniqueness_of :post, scope: %i[obj_type obj_id]

  def notif_new_mention(author)
    if %w[User Project].include?(obj.class.name)
      Notification.create(
        target: obj,
        category: :mention,
        type: 'new_mention',
        object: post,
        metadata: { author_id: author.id }
      )
    else
      Notification.for_group(
        :members,
        args: [obj],
        attrs: {
          category: :mention,
          type: 'new_clap',
          object: post, metadata: {
            author_id: author.id
          }
        }
      )
    end
  end
end
