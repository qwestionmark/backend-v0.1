class Authorship < ApplicationRecord
  belongs_to :user
  belongs_to :authorable, polymorphic: true
end
