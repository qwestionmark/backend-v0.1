# frozen_string_literal: true

class Datum < ApplicationRecord
  has_paper_trail
  has_many :sources, as: :sourceable
  has_one :license, as: :licenseable
  has_many :datafields

  has_one_attached :document

  belongs_to :author, foreign_key: 'author_id', class_name: 'User'
  belongs_to :dataset

  # Attributes
  # :title
  # :description
  # :url
  # :format CSV / JSON / XLSX ...
  # :filename
  #
  # :provider_id if there is an ID for the external provider
  # :provider_created_at The date of creation on the provider
  # :provider_updated_at The date of modification if any
  # :provider_version The version if any
end
