# frozen_string_literal: true

class Community < ApplicationRecord
  resourcify
  notification_object
  include AlgoliaSearch
  include RelationHelpers
  include Coordinates
  include Utils
  include Ressourceable
  include RecsysHelpers
  include NotificationsHelpers
  include Bannerable
  include Linkable
  include Feedable
  include Membership
  include Geocodable
  include Skillable
  include Interestable

  has_and_belongs_to_many :community_tags
  belongs_to :creator, foreign_key: 'creator_id', class_name: 'User'
  has_many :challenges_communities
  has_many :challenges, through: :challenges_communities

  has_many :externalhooks, as: :hookable

  enum status: %i[active archived]

  before_create :create_coordinates

  before_create :sanitize_description
  before_update :sanitize_description

  validates :short_title, uniqueness: true

  geocoded_by :make_address
  after_validation :geocode

  algoliasearch disable_indexing: !Rails.env.production?, unless: :archived? do
    use_serializer Api::CommunitySerializer
    geoloc :latitude, :longitude
  end

  def frontend_link
    "/community/#{id}"
  end

  def notif_pending_join_request(author)
    Notification.create(
      target: author,
      category: :membership,
      type: 'pending_join_request',
      object: self,
      metadata: { author_id: author.id }
    )
  end

  def notif_pending_join_request_approved(member)
    Notification.create(
      target: member,
      category: :membership,
      type: 'pending_join_request_approved',
      object: self
    )
  end

  private

  def default_banner_url
    ActionController::Base.helpers.image_url('default-group.jpg')
  end
end
