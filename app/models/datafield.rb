# frozen_string_literal: true

class Datafield < ApplicationRecord
  belongs_to :datum

  # Attributes
  # :name
  # :title
  # :description
  # :format "YYYY-MM" or any other formating metadata
  # :unit
end
