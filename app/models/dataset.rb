# frozen_string_literal: true

class Dataset < ApplicationRecord
  has_paper_trail
  notification_object
  include RecsysHelpers
  include AlgoliaSearch


  has_many :data, dependent: :destroy
  has_many :sources, as: :sourceable, dependent: :destroy
  has_many :tags, as: :tagable, dependent: :destroy
  has_one :license, as: :licenseable, dependent: :destroy

  belongs_to :author, foreign_key: 'author_id', class_name: 'User'
  belongs_to :datasetable, polymorphic: true, optional: true

  # Attributes
  # :title
  # :description
  # :type this is the dataset type
  # :provider This is the provider of the data JOGL Data.gov KapCode etc
  # :provider_type This is the type of provider JOGL CKAN KapCode
  # :provider_url This is the URL of the external provider data.jogl.io etc
  # :provider_author The author from the dataset on the provider context
  # :provider_authoremail The author email if available
  # :provider_created_at The date of creation on the provider
  # :provider_updated_at The date of modification if any
  # :provider_version The version if any

  algoliasearch disable_indexing: !Rails.env.production? do
    use_serializer Api::DatasetSerializer
  end

  validates :type, presence: true
  validates :url, presence: true, unless: :customdataset

  def customdataset
    type == Datasets::CustomDataset
  end
end
