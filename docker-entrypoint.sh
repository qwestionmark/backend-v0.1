#!/bin/bash

set -e

# if python3 ./configure.py; then
  # echo "Starting Logstash..."
  # /usr/share/logstash/bin/logstash -f config/logstash.conf &
# fi

rm -rf tmp/*

export PATH=$HOME/node_modules/.bin:$PATH

if [ $1 = 'server' ]; then
  echo "Starting the server with environment: $RAILS_ENV"
  bundle exec rails s -p $PORT -b 0.0.0.0
elif [ $1 = 'sidekiq' ]; then
  echo "Starting sidekiq"
  bundle exec sidekiq -C ./config/sidekiq.yml
else
  exec "$@"
fi
