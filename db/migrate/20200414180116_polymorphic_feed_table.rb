# frozen_string_literal: true

class PolymorphicFeedTable < ActiveRecord::Migration[5.2]
  def up
    remove_index :feeds, :community_id
    remove_index :feeds, :project_id
    remove_index :feeds, :user_id

    add_column :feeds, :feedable_id, :integer
    rename_column :feeds, :object_type, :feedable_type

    execute <<-SQL
      UPDATE feeds SET
        feedable_id = coalesce(user_id, project_id, community_id, challenge_id, program_id, need_id);
      UPDATE feeds SET
        feedable_type = INITCAP(feedable_type);
    SQL

    remove_column :feeds, :user_id, :integer
    remove_column :feeds, :project_id, :integer
    remove_column :feeds, :community_id, :integer
    remove_column :feeds, :challenge_id, :integer
    remove_column :feeds, :program_id, :integer
    remove_column :feeds, :need_id, :integer

    add_index :feeds, %w[feedable_type feedable_id]
  end

  def down
    remove_index :feeds, %w[feedable_type feedable_id]

    add_column :feeds, :user_id, :integer
    add_column :feeds, :project_id, :integer
    add_column :feeds, :community_id, :integer
    add_column :feeds, :challenge_id, :integer
    add_column :feeds, :program_id, :integer
    add_column :feeds, :need_id, :integer

    # revert to sparse table
    %w[Challenge Community Need Program Project User].each do |type|
      execute("update feeds set #{type}_id = feedable_id where feedable_type = '#{type}'")
    end

    # revert the type back to all lower case like they were previously
    execute('update feeds set feedable_type = LOWER(feedable_type)')

    rename_column :feeds, :feedable_type, :object_type
    remove_column :feeds, :feedable_id, :integer

    add_index :feeds, :community_id
    add_index :feeds, :project_id
    add_index :feeds, :user_id
  end
end
