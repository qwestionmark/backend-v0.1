# frozen_string_literal: true

class MigrateUserCountry < ActiveRecord::Migration[5.2]
  def change
    spell_checker = DidYouMean::SpellChecker.new(dictionary: User::VALID_COUNTRY_NAMES)

    User.where.not(country: [nil, '']).find_each(batch_size: 100) do |user|
      country = spell_checker.correct(user.country).first
      user.update(country: country) if country && country != user.country
    end
  end
end
