# frozen_string_literal: true

class RemoveEmailNotificationsEnabledFromUser < ActiveRecord::Migration[5.2]
  def change
    remove_column :users, :email_notifications_enabled
  end
end
