# frozen_string_literal: true

class CreateTags < ActiveRecord::Migration[5.2]
  def change
    create_table :tags do |t|
      t.string :name
      t.bigint :tagable_id
      t.string :tagable_type
      t.timestamps
    end
  end
end
