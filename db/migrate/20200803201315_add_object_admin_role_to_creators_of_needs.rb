# frozen_string_literal: true

class AddObjectAdminRoleToCreatorsOfNeeds < ActiveRecord::Migration[5.2]
  def change
    # find all the needs
    Need.all.each do |need|
      # need.user is the creator of the need (as assigned in the controller)
      # so we add the admin role to any needs creators who don't have it
      # so that we match current controller behavior
      need.user.add_role(:admin, need)
    end
  end
end
