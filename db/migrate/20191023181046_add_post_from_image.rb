# frozen_string_literal: true

class AddPostFromImage < ActiveRecord::Migration[5.2]
  def change
    add_column :posts, :from_image, :string
  end
end
