# frozen_string_literal: true
# License.find_or_create_by!({
#   title: "Creative Common",
#   short_title: "CC",
#   version: "1.0",
#   attr: "0",
#   logo_url: "https://licensebuttons.net/l/publicdomain/88x31.png",
#   url: "https://creativecommons.org/publicdomain/zero/1.0/",
#   url_legal: "https://creativecommons.org/publicdomain/zero/1.0/legalcode"})
#
# License.find_or_create_by!({
#   title: "Creative Common",
#   short_title: "CC",
#   version: "4.0",
#   attr: "BY",
#   logo_url: "https://licensebuttons.net/l/by/4.0/88x31.png",
#   url: "https://creativecommons.org/licenses/by/4.0/",
#   url_legal: "https://creativecommons.org/licenses/by/4.0/legalcode"})
#
# License.find_or_create_by!({
#   title: "Creative Common",
#   short_title: "CC",
#   version: "4.0",
#   attr: "BY-SA",
#   logo_url: "https://licensebuttons.net/l/by-sa/4.0/88x31.png",
#   url: "https://creativecommons.org/licenses/by-sa/4.0/",
#   url_legal: "https://creativecommons.org/licenses/by-sa/4.0/legalcode"})
#
# License.find_or_create_by!({
#   title: "Creative Common",
#   short_title: "CC",
#   version: "4.0",
#   attr: "BY-ND",
#   logo_url: "https://licensebuttons.net/l/by-nd/4.0/88x31.png",
#   url: "https://creativecommons.org/licenses/by-nd/4.0/",
#   url_legal: "https://creativecommons.org/licenses/by-nd/4.0/legalcode"})
#
# License.find_or_create_by!({
#   title: "Creative Common",
#   short_title: "CC",
#   version: "4.0",
#   attr: "BY-NC",
#   logo_url: "https://licensebuttons.net/l/by-nc/4.0/88x31.png",
#   url: "https://creativecommons.org/licenses/by-nc/4.0/",
#   url_legal: "https://creativecommons.org/licenses/by-nc/4.0/legalcode"})
#
# License.find_or_create_by!({
#   title: "Creative Common",
#   short_title: "CC",
#   version: "4.0",
#   attr: "BY-NC-SA",
#   logo_url: "https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png",
#   url: "https://creativecommons.org/licenses/by-nc-sa/4.0/",
#   url_legal: "https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode"})
#
# License.find_or_create_by!({
#   title: "Creative Common",
#   short_title: "CC",
#   version: "4.0",
#   attr: "BY-NC-ND",
#   logo_url: "https://licensebuttons.net/l/by-nc-nd/4.0/88x31.png",
#   url: "https://creativecommons.org/licenses/by-nc-nd/4.0/",
#   url_legal: "https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode"})
#
# License.find_or_create_by!({
#   title: "GNU General Public License",
#   short_title: "GPL",
#   version: "3.0",
#   attr: "",
#   logo_url: "https://www.gnu.org/graphics/gplv3-88x31.png",
#   url: "https://www.gnu.org/licenses/quick-guide-gplv3.html",
#   url_legal: "https://www.gnu.org/licenses/gpl-3.0.en.html"})
#
# License.find_or_create_by!({
#   title: "GNU Lesser General Public License",
#   short_title: "LGPL",
#   version: "3.0",
#   attr: "",
#   logo_url: "https://www.gnu.org/graphics/lgplv3-88x31.png",
#   url: "https://www.gnu.org/licenses/quick-guide-gplv3.html",
#   url_legal: "https://www.gnu.org/licenses/lgpl-3.0.html"})
#
# License.find_or_create_by!({
#   title: "GNU Affero General Public License",
#   short_title: "AGPL",
#   version: "3.0",
#   attr: "",
#   logo_url: "https://www.gnu.org/graphics/agplv3-88x31.png",
#   url: "https://www.gnu.org/licenses/quick-guide-gplv3.html",
#   url_legal: "https://www.gnu.org/licenses/agpl-3.0.html"})
#
# License.find_or_create_by!({
#   title: "MIT License",
#   short_title: "MIT",
#   version: "1.0",
#   attr: "",
#   logo_url: "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/License_icon-mit-88x31-2.svg/88px-License_icon-mit-88x31-2.svg.png",
#   url: "https://choosealicense.com/licenses/mit/",
#   url_legal: "https://en.wikipedia.org/wiki/MIT_License"})
